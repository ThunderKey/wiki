# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) { |repo_name| "https://github.com/#{repo_name}" }

gem 'jekyll', '~> 3.8.5'

# If you want to use GitHub Pages, remove the "gem "jekyll"" above and
# uncomment the line below. To upgrade, run `bundle update github-pages`.
# gem "github-pages", group: :jekyll_plugins

# If you have any plugins, put them here!
group :jekyll_plugins do
  gem 'jekyll-feed', '~> 0.6'
  gem 'jekyll-mermaid'
  gem 'jekyll-seo-tag'

  gem 'coffee-script'
  gem 'jekyll-assets'
  gem 'sass'
  gem 'therubyracer'
  gem 'uglifier'
end

source 'https://rails-assets.org' do
  gem 'rails-assets-foundation-sites'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i(mingw mswin x64_mingw jruby)

# Performance-booster for watching directories on Windows
gem 'wdm', '~> 0.1.0' if Gem.win_platform?

gem 'ThunderKey-rubocop'
