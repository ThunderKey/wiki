---
title: Ruby Gems
layout: page
---

## Web
### Backend

* [devise](https://github.com/plataformatec/devise) simple logins

### ORM
* [ancestry](https://github.com/stefankroes/ancestry) ActiveRecord tree structures
* [geokit-rails](https://github.com/geokit/geokit-rails) ActiveRecord with locations

### Frontend

* [best\_in\_place](https://github.com/bernat/best_in_place) inline form editing
* [breadcrumbs\_on\_rails](https://github.com/weppos/breadcrumbs_on_rails) simple breadcrumbs
* [activeadmin](https://github.com/activeadmin/activeadmin) data management
* [rails\_admin](https://github.com/sferik/rails_admin) data management
* [administrate](https://github.com/thoughtbot/administrate) data management without DSL and rails-like
* [letter\_opener](https://github.com/ryanb/letter_opener) preview mails in the browser instead of sending them

## CMD
* [ruby-progressbar](https://github.com/jfelchner/ruby-progressbar) a simple progress bar for shell scripts
