---
title: Floating-Point Arithmetic
layout: page
---

## Problems
### Precision
Some numbers in the base 10 System can't be put in the base 2 system without losing precision. For example $0.3$ is represented as the fraction of $\frac{5404319552844595}{18014398509481984}$ as a `double` and $\frac{5033165}{16777216}$ as a `float`

Example in Ruby:

```ruby
0.3.to_r # => (5404319552844595/18014398509481984)
'0.3'.to_r # => (3/10)
```

The problem is the definition of `double` and `float` in [IEEE 754](#ieee-754). I currently know of two solutions:

1. **Rational data types** These save both the nominator and the denominator as two separate integers. With that you can save any type of fraction. Example: [Ruby Rational](https://ruby-doc.org/core-2.5.3/Rational.html)
1. **Decimal data types** These use base 10 instead of base 2. With that you can save large base 10 numbers but you can still be imprecise. Example: [Ruby BigDecimal](https://ruby-doc.org/stdlib-2.5.3/libdoc/bigdecimal/rdoc/BigDecimal.html)

## IEEE 754
See [IEEE 754](https://en.wikipedia.org/wiki/IEEE_754)

All formats consist of:

* Base $b$, either 2 or 10
* Precision $p$
* Exponent range $emin$ to $emax$; where $emin = 1 - emax$

All format have the following items:

* $-\infty$
* $+\infty$
* $qNaN$ quiet NaN (default); behaves like a number
* $sNaN$ signaling NaN; causes an "invalid" exception
* Numbers in the format $(-1)^s \times c \times b^q$ where:
  * $s =$ a sign, zero or one
  * $c =$ a significand in the range $0$ and $b^p$
  * $q =$ a number that matches $emin ≤ q+p−1 ≤ emax$

### Common types

| Name | Common Name | in C | $b$ | $p$ | $emin$ | $emax$ |
| -------- | --------------------- | ----- | ----- | ----- |---------- | ----------- |
| [binary32](https://en.wikipedia.org/wiki/Single-precision_floating-point_format) | Single precision | `float` | $2$ | $24$ | $-126$ | $+127$ |
| [binary64](https://en.wikipedia.org/wiki/Double-precision_floating-point_format) | Double precision | `double` | $2$ | $53$ | $-1022$ | $+1023$ |
| [decimal64](https://en.wikipedia.org/wiki/Decimal64_floating-point_format) | | | $10$ | $16$ | $-383$ | $+384$ |

### Special cases

$$\infty \times a = \begin{cases}
  \infty & \mathrm{\ if \ } a > 0 \\\\
  -\infty & \mathrm{\ if \ } a < 0 \\\\
  NaN & \mathrm{\ if \ } a = 0 \\\\
\end{cases}$$

$$\frac{a}{0} = \begin{cases}
  +\infty & \mathrm{\ if\ } a > 0 \\\\
  -\infty & \mathrm{\ if\ } a < 0 \\\\
  NaN & \mathrm{\ if\ } a = 0
\end{cases}$$
