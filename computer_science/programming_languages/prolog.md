---
title: Prolog
layout: page
---

## Basics

### Terms

{% mermaid %}
graph TD;
  Terms --> st["Simple Terms"]
  Terms --> ct["Complex Terms"]
  st --> Constants
  st --> Variables
  Constants --> Atoms
  Constants --> Numbers
{% endmermaid %}

<dl class='inline'>
  <dt>Complex Terms</dt>
  <dd><code>even(2), h(X,f(b)), ...</code></dd>
  <dt>Variables</dt>
  <dd><code>X, Y, _tag, ...</code></dd>
  <dt>Atoms</dt>
  <dd><code>a, b, c, 'A', ...</code></dd>
  <dt>Numbers</dt>
  <dd><code>1, 2, -3, 4, ...</code></dd>
</dl>

### Facts

```
happy(yolanda).
listens2music(mia).
outInNature(peter).
```

### Rules

```
happy(X) :- listens2music(X).
happy(X) :- listens2music(X); outInNature(X). % OR
reallyHappy(X) :- listens2music(X), outInNature(X). % AND
```

### Lists

```
[X,Y|L] = [1,2,3,4,5] % X = 1, Y = 2, L = [3,4,5]

member(X, [X|_]).
member(X, [_|T]):- member(X, T).
member(c, [a,b,c]). % true; false.
member(x, [a,b,c]). % false.

append([],L,L).
append([H|L1],L2,[H|L3):- append(L1,L2,L3).
```
