---
title: LaTeX
layout: page
---

<ul class='accordion' data-accordion data-multi-expand="true">
  {% for group_data in site.latex_symbols %}
    {% assign group_key = group_data[0] %}
    {% assign symbols = group_data[1] %}
    <li class="accordion-item is-active" data-accordion-item>
      <a href="#" class="accordion-title">{{group_key}}</a>

      <div class="accordion-content" data-tab-content>
        <table>
          <thead>
            <tr>
              <th>Symbol</th>
              <th>$\LaTeX$</th>
              <th>Comment</th>
            </tr>
          </thead>
          <tbody>
            {% for symbol_data in symbols %}
              {% assign symbol = symbol_data[0] %}
              {% assign comment = symbol_data[1] %}
              <tr>
                <td>${{symbol}}$</td>
                <td><code>{{symbol}}</code></td>
                <td>{{comment}}</td>
              </tr>
            {% endfor %}
          </tbody>
        </table>
      </div>
    </li>
  {% endfor %}
</ul>

## References

* [List of LaTeX mathematical symbols](https://oeis.org/wiki/List_of_LaTeX_mathematical_symbols)
