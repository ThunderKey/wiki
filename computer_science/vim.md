---
title: VIM
layout: page
---

## Cursor

| Command | Action |
|---|---|
| `H` | Move the cursor to the **top** part of the screen |
| `M` | Move the cursor to the **middle** part of the screen |
| `L` | Move the cursor to the **lower** part of the screen |
| `zt` | Move the screen so that the cursor is in the **top** part of the screen |
| `zz` | Move the screen so that the cursor is in the **middle** part of the screen |
| `zb` | Move the screen so that the cursor is in the **bottom** part of the screen |
| `Ctrl-b` | Move a **large** step up |
| `Ctrl-u` | Move a **medium** step up |
| `Ctrl-y` | Move a **small** step up |
| `Ctrl-f` | Move a **large** step down |
| `Ctrl-d` | Move a **medium** step down |
| `Ctrl-e` | Move a **small** step down |

## Movements

| Command | Action |
|---|---|
| `Ctrl-o` | Move to a older cursor position |
| `Ctrl-i` | Move to a newer cursor position |
| `g;` | Move to a older change |
| `g,` | Move to a newer change |
