#= require jquery
#= require foundation-sites

$ ->
  $(document).foundation()

  MathJax.Hub.Queue ['Typeset', MathJax.Hub]
  MathJax.Hub.Config(
    tex2jax: {
      inlineMath: [['$','$'], ['\\(','\\)']],
      processEscapes: true
    },
  )
