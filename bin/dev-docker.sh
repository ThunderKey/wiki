#!/bin/bash

BIN_DIR="$(cd $(dirname $0); pwd)"
ROOT_DIR="$(dirname "$BIN_DIR")"

vrun() {
  echo $@
  $@
}

COMPOSE_ARGS="-f $ROOT_DIR/dockerfiles/docker-compose.yml"

docker_cmd() { vrun docker-compose $COMPOSE_ARGS "$@"; }

docker_down() { docker_cmd down --volumes; }

if [[ "$@" == "" ]]; then
  docker_cmd up --abort-on-container-exit
elif [[ "$@" == "stop" ]] || [[ "$@" == "down" ]]; then
  docker_down
elif [[ "$@" == "build" ]]; then
  docker_down
  docker_cmd build
else
  [[ "$CONTAINER" == "" ]] && CONTAINER="web"
  docker_cmd run --rm "$CONTAINER" "$@"
fi
