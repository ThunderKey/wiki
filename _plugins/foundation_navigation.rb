# frozen_string_literal: true

require_relative 'navigation_tree'

module Jekyll
  class FoundationNavigation < Liquid::Tag
    def render context
      site = context.registers[:site]
      @tree = NavigationTree.fetch context
      <<-HTML
<div class="top-bar">
  <div class="top-bar-left">
    <ul class="menu">
      <li class="menu-title">
        <a href="#{context.invoke 'relative_url', '/'}">#{context.invoke 'escape', site.config['title']}</a>
      </li>
    </ul>
  </div>
  <div class="top-bar-right">
      #{render_node @tree.root_node, context}
  </div>
</div>
HTML
    end

    def render_node node, context, root: true
      content = [
        root ? '' : %Q{<a href="#{context.invoke 'relative_url', node.path}">#{node.title}</a>},
      ]
      return content.join if node.children.empty?

      content << (root ? '<ul class="dropdown menu" data-dropdown-menu>' : '<ul class="menu">')
      node.children.each do |child|
        content << "<li>#{render_node child, context, root: false}</li>"
      end
      content << '</ul>'
      content.join
    end
  end

  Liquid::Template.register_tag('foundation_navigation', FoundationNavigation)
end
