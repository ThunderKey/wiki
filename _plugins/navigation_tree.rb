# frozen_string_literal: true

module Jekyll
  class NavigationTree
    def self.fetch context
      @trees ||= {}
      pages = context.registers[:site].pages
      @trees[pages] ||= NavigationTree.new pages
    end

    IGNORED_PAGES = %w(feed.xml).freeze

    attr_reader :root_node

    def initialize pages
      @root_node = fetch_nodes pages
    end

    def fetch_nodes pages
      nodes = pages.map {|page| fetch_node page }.reject(&:nil?).sort
      root_node = nodes.first
      node_hierarchy = [root_node]
      nodes[1..-1].each do |node|
        node_hierarchy.reverse_each do |parent_node|
          if parent_node.includes? node
            parent_node.children << node
            node_hierarchy << node
            break
          end

          node_hierarchy.pop
        end
      end

      root_node
    end

    def fetch_node page
      # exclude all pages that are hidden in front-matter
      return nil unless page.data['hide_nav'] != true && page.data['sitemap'] != false

      path = page.url.gsub %r{\A/}, ''
      Node.new path, page.data['title']
    end

    class Node
      attr_reader :path, :paths, :real_paths, :title, :children

      def initialize path, title, children = []
        @path = path
        @paths = path.split '/'
        @real_paths = paths.map {|part| part.gsub(/\.html\z/, '') }
        @title = title
        @children = children
      end

      def includes? other
        other.real_paths.take(real_paths.size) == real_paths
      end

      def <=> other
        real_paths <=> other.real_paths
      end
    end
  end
end
